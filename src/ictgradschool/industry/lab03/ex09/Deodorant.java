package ictgradschool.industry.lab03.ex09;

public class Deodorant {

    private String brand;
    private String fragrance;
    private boolean rollOn;
    private double price;

    public Deodorant(String brand, String fragrance,
                     boolean rollOn, double price) {
        // Constructor arguements

        this.brand = brand;
        this.fragrance = fragrance;
        this.rollOn = rollOn;
        this.price = price;
    }

    public String toString() {
        String info = brand + " " + fragrance;
        if (rollOn) {
            info = info + " Roll-On";
        } else {
            info = info + " Spray";
        }
        info +=  " Deodorant, \n$" + price;
        return info;
    }

    // TODO Implement all methods below this line.

    public double getPrice() { /*accessor*/
        return price;
    }

    public String getBrand() { /*accessor*/
        return brand;
    }

    public boolean isRollOn() {
        return rollOn;
    }

    public String getFragrance() { /*accessor*/
        return this.fragrance;
    }

    public void setPrice(double price) { /*mutator*/
        this.price = price;

    }

    public void setBrand(String brand) { /*mutator*/
        this.brand = brand;

    }

    public void setFragrance(String fragrance) { /*mutator*/
        this.fragrance = fragrance;

    }

    public boolean isMoreExpensiveThan(Deodorant other) {
        if (other instanceof Deodorant) {
            Deodorant otherD = (Deodorant) other;
            return this.price > otherD.price;
        }
        return false;

        /* return this.price > other.price; */
    }

}