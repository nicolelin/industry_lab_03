package ictgradschool.industry.lab03.ex03;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a sentence, then prints out the sentence with a random character
 * missing. The program is to be written so that each task is in a separate method. See the comments below for the
 * different methods you have to write.
 */

//

public class ExerciseThree {

    private void start() {

        String sentence = getSentenceFromUser();

        // System.out.println("The current sentence is: " + sentence);

        int randomPosition = getRandomPosition(sentence);

        printCharacterToBeRemoved(sentence, randomPosition);

        String changedSentence = removeCharacter(sentence, randomPosition);

        printNewSentence(changedSentence);
    }

    /**
     * Gets a sentence from the user.
     * @return
     */
    private String getSentenceFromUser() {

        // TODO Prompt the user to enter a sentence, then get their input.

        System.out.println("Please enter a sentence:");
        String sentence = (Keyboard.readInput());

        return sentence;
    }

    /**
     * Gets an int corresponding to a random position in the sentence.
     */
    private int getRandomPosition(String sentence) {

        // TODO Use a combination of Math.random() and sentence.length() to get the desired result.

        // Calculate length of sentence
        // Generate random number for index int i between 1 and sentence.length() // within range sentence.length()

        // int random = (int) (Math.random() * (sentence.length() - 1 + 1) + 1 );

        int randomPosition = (int) (Math.random() * sentence.length());

        return randomPosition;
    }

    /**
     * Prints a message stating the character to be removed, and its position.
     */
    private void printCharacterToBeRemoved(String sentence, int position) {

        // TODO Implement this method

        // Lookup char at index i (random)
        // Print message, return character and index position

        System.out.println("Removing " + sentence.charAt(position) + " from position " + position);

    }

    /**
     * Removes a character from the given sentence, and returns the new sentence.
     */
    private String removeCharacter(String sentence, int position) {

        // TODO Implement this method

        // Print from 1st character until the position to remove, concat the position from above (add the rest of the sentence on to the end)

        String changedSentence = sentence.substring(0,position) + sentence.substring(position+1);

        return changedSentence;

    }

    /**
     * Prints a message which shows the new sentence after the removal has occured.
     */
    private void printNewSentence(String changedSentence) {

        // TODO Implement this method

        System.out.println("The new sentence is: " + changedSentence);
    }

    public static void main(String[] args) {
        ExerciseThree ex = new ExerciseThree();
        ex.start();
    }
}
