package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

import java.util.Scanner;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */

    // Set integers required for memory
    // Prompt user to enter 2 numbers // Prompt lower then upper // else /** // Find Math.min and Math.max between integers 1 and 2 **/ //
    // Convert input string into int
    // Generate 3 random numbers within range of upper and lower - create method, call 3 times // take the top number, subtract the lower number, plus 1
    // Return output
    // Find lowest of the 3 random numbers
    // Return Lowest of the 3 random numbers

    int num1;
    int num2;
    // int max;
    // int min;
    int rand1;
    int rand2;
    int rand3;
    int randmin;

    private void start() {

        System.out.println("Please enter lower number:");
        int num1 = Integer.parseInt(Keyboard.readInput());
        System.out.println("Please enter higher number:");
        int num2 = Integer.parseInt(Keyboard.readInput());

        // max = Math.max(num1,num2);
        // min = Math.min(num1,num2);

        rand1 = generateRandomNumber(num1, num2);
        rand2 = generateRandomNumber(num1, num2);
        rand3 = generateRandomNumber(num1, num2);

        System.out.println("3 randomly generated numbers between given range: " + rand1 + " " + rand2 + " " + rand3);

        int randmin = Math.min(rand1, Math.min(rand2, rand3));
        System.out.println("The smallest random number is: " + randmin);

    }

    public int generateRandomNumber(int numLower, int numUpper) {
        int random = (int) (Math.random() * (numUpper - numLower + 1) + numLower);
        System.out.println(random);

        return random;
    }

    /**
     * Program entry point. Do not edit.
     */

    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
