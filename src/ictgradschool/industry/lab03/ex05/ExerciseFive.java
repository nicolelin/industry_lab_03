package ictgradschool.industry.lab03.ex05;

/**
 * Created by anhyd on 3/03/2017.
 */
public class ExerciseFive {

    /**
     * Runs an example program.
     */

    // Extract and return specified substring of characters from String letters
    // start from index 0, grab up to including index 5 (0,1,2,3,4,5)
    // Print 3 rows -> grab from end of last substring, increment by 6 more chars
    // Extract and return left diagonal -> need to read characters just printed?
    // Print extracted diagonal

    private void start() {
        String letters = "X X O O X O X O X ";

        String row1 = getRow(letters, 1);

        String row2 = getRow(letters, 2);

        String row3 = getRow(letters, 3);

        printRows(row1, row2, row3);

        String leftDiagonal = getLeftDiagonal(row1, row2, row3);

        printDiagonal(leftDiagonal);
    }

    /**
     * TODO Implement this
     */
    public String getRow(String letters, int row) {
        // return letters.substring((row - 1) * 6, row * 6)

        if (row == 1) {
            return letters.substring(0,6);
        }
        else if (row == 2) {
            return letters.substring(6,12);
        }
        else if (row == 3) {
            return letters.substring(12,18);
        }

        return "";
    }

    /**
     * TODO Implement this
     */
    public void printRows(String row1, String row2, String row3) {

        System.out.print(row1 + "\n");
        System.out.print(row2 + "\n");
        System.out.print(row3 + "\n");
    }

    /**
     * TODO Implement this
     */
    public String getLeftDiagonal(String row1, String row2, String row3) {

        String result = row1.charAt(0) + " " + row2.charAt(2) + " " + row3.charAt(4);

        return result;
    }

    /**
     * TODO Implement this
     */
    public void printDiagonal(String leftDiagonal) {

        System.out.println("Diagonal: " + leftDiagonal);

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFive ex = new ExerciseFive();
        ex.start();
    }
}
