package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        // TODO Use other methods you create to implement this program's functionality.

        String decimalInput = readDecimalInput();
        int decimalPoints = readDecimalPoints();
        String result = truncatedDecimalNumber(decimalInput, decimalPoints);
        printTruncatedNumber(result);

    }

        // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard

        /** Get input from user for number **/

    private String readDecimalInput() {
        System.out.println("Please input the number with decimal points:");
        String decimalInput = Keyboard.readInput();
        return decimalInput;
    }

    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard

    private int readDecimalPoints() {
        System.out.println("Please input number of decimal places to keep:");
        int decimalPoints = Integer.parseInt(Keyboard.readInput());
        return decimalPoints;
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's

    private String truncatedDecimalNumber(String /* decimalInput */ number, int /* decimalPoints */ DP) {
        int decimalIndex = number.indexOf(".");
        decimalIndex = decimalIndex + 1 + DP; //Plus 1 to get where the number starts at after . and add DP end point to truncate chars
        String results = number.substring(0, decimalIndex);
        // String results = string.substring(0,decimalIndex + 1 + decimalPoints);
        return results;
    }

    // TODO Write a method which prints the truncated amount

    private void printTruncatedNumber(String results) {
        System.out.print("Truncated number is: " + results);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}