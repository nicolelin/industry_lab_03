package ictgradschool.industry.lab03.ex08;

import ictgradschool.Keyboard;

/**
 * Created by ylin183 on 16/03/2017.
 */
public class CalculateVolume { /** Missing open curly bracket **/

        public void /** Missing void **/ start() {

            double radius; /** Missing ; **/

            System.out.println("\"Volume of a Sphere\"" /** Fixed last quote mark inside string **/);

                    System.out.println("Enter the radius:" /** Missing close quote mark **/);

                            radius = Integer.parseInt(Keyboard.readInput()/** Missing () **/);

            double /** changed to double **/ volume = 4.0 /** added .0 to change into double **/ / 3 * Math.PI * Math.pow(radius,2);

            System.out.println("Volume: " + /** changed , to + **/ volume);

        }

    public static void main(String[] args) {
        CalculateVolume ex = new CalculateVolume();
        ex.start();
    }

}